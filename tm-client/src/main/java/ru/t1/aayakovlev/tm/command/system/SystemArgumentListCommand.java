package ru.t1.aayakovlev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.command.AbstractCommand;

import java.util.Collection;

public final class SystemArgumentListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    public static final String DESCRIPTION = "Show argument description.";

    @NotNull
    public static final String NAME = "argument";

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENT LIST]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        commands.stream()
                .filter((c) -> c.getArgument() != null)
                .filter((c) -> !c.getArgument().isEmpty())
                .forEachOrdered((c) -> System.out.println(c.getArgument() + ": " + c.getDescription()));
    }

}
