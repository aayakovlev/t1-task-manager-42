package ru.t1.aayakovlev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;

public interface ConnectionService {

    @NotNull
    SqlSession getSqlSession();

}
