package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.EmailEmptyException;
import ru.t1.aayakovlev.tm.exception.field.LoginEmptyException;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;

import java.util.List;

public interface UserService {

    void clear();

    @NotNull
    UserDTO create(@Nullable final String login, @Nullable final String password) throws AbstractException;

    @NotNull
    UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException;

    @NotNull
    UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws AbstractException;

    @Nullable
    UserDTO findByLogin(@Nullable final String login) throws AbstractException;

    @Nullable
    UserDTO findByEmail(@Nullable final String email) throws AbstractException;

    boolean isLoginExists(@Nullable final String login) throws LoginEmptyException;

    boolean isEmailExists(@Nullable final String email) throws EmailEmptyException;

    @NotNull
    UserDTO lockUserByLogin(@Nullable final String login) throws AbstractException;

    void removeByLogin(@Nullable final String login) throws AbstractException;

    void removeByEmail(@Nullable final String email) throws AbstractException;

    void removeById(@Nullable final String id) throws AbstractException;

    @NotNull
    UserDTO setPassword(@Nullable final String id, @Nullable final String password) throws AbstractException;

    @NotNull
    UserDTO unlockUserByLogin(@Nullable final String login) throws AbstractException;

    @NotNull
    UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstname,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws AbstractException;

    @NotNull
    UserDTO findById(@Nullable final String userId) throws AbstractException;

    @NotNull
    List<UserDTO> findAll();

    UserDTO save(@NotNull final UserDTO user);

    void set(@Nullable final List<UserDTO> users);

}
