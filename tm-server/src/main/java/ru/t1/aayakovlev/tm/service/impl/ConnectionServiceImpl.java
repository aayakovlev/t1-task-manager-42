package ru.t1.aayakovlev.tm.service.impl;

import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.mybatis.caches.hazelcast.LoggingHazelcastCache;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.*;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.DatabaseProperty;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

import static org.hibernate.cfg.Environment.*;

public final class ConnectionServiceImpl implements ConnectionService {

    // TODO: 28.11.2022 добавить работу с h2 для тестов

    @NotNull
    private final DatabaseProperty databaseProperty;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionServiceImpl(@NotNull final DatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.sqlSessionFactory = getSqlSessionFactory();
        this.entityManagerFactory = factory();
        initAdminUser();
    }

    @NotNull
    @Override
    @SneakyThrows
    public SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

    @NotNull
    @SneakyThrows
    private SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String username = databaseProperty.getDatabaseUser();
        @NotNull final String password = databaseProperty.getDatabasePassword();
        @NotNull final String url = databaseProperty.getDatabaseURL();
        @NotNull final String driver = databaseProperty.getDatabaseDriver();
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, username, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(ProjectRepository.class);
        configuration.addMapper(TaskRepository.class);
        configuration.addMapper(UserRepository.class);
        configuration.addMapper(SessionRepository.class);
        configuration.setCacheEnabled(true);
        configuration.addCache(new LoggingHazelcastCache("tm"));
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    @NotNull
    private EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(URL, databaseProperty.getDatabaseURL());
        settings.put(USER, databaseProperty.getDatabaseUser());
        settings.put(PASS, databaseProperty.getDatabasePassword());
        settings.put(DIALECT, databaseProperty.getDatabaseDialect());
        settings.put(HBM2DDL_AUTO, databaseProperty.getDatabaseHBM2DLL());
        settings.put(SHOW_SQL, databaseProperty.getDataBaseShowSql());
        settings.put(DEFAULT_SCHEMA, databaseProperty.getDataBaseSchema());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(User.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    private void initAdminUser() {
        @NotNull final SqlSession session = getSqlSession();
        @NotNull final UserRepository userRepository = session.getMapper(UserRepository.class);
        if (userRepository.findByLogin("admin") == null) {
            userRepository.save(new UserDTO("admin", Role.ADMIN, "5759bb213e44ab5d7527cb0c29ffb911"));
            System.out.println("Admin user with password `admin` initialised");
        }
        session.commit();
    }

}
