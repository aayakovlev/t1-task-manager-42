package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;

import java.util.List;

public interface ProjectService {

    void clear();

    void clear(@Nullable final String userId) throws UserIdEmptyException;

    int count(@Nullable final String userId) throws AbstractException;

    @NotNull
    List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final Sort sort) throws UserIdEmptyException;

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    ProjectDTO findById(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    void removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    @NotNull
    ProjectDTO save(@NotNull final ProjectDTO project);

    @NotNull
    ProjectDTO changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException;

    @NotNull
    ProjectDTO create(@Nullable final String userId, @Nullable final String name) throws AbstractFieldException;

    @NotNull
    ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractFieldException;

    @NotNull
    ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException;

    void set(@Nullable final List<ProjectDTO> projects);

}
